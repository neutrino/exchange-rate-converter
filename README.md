# ExchangeRateConverter
Initially, I started this task with an idea of a gem and wanted to have a database adaptor that can be plugged into any ActiveModel object. But, later with the time constraint, I decided to build it as a library and then later try to make a gem out of it.

## Additional Libraries
The dependencies for this ExchangeRateConverter are listed in the .gemspec then normal Gemfile. It usages bundler for the gem management and relevant library can be installed with 'bundle install' command.

## Database
- It usages SQLite for the development and active record as an ORM.
- The configuration for the database can be managed from 'config/database.yml' file. The project includes sample file as  'config/database_example.yml' and can be copied and renamed to database.yml.
- Using MySQL or PostgreSQL will require some changes in the library and may need administrative access to create database

## Rake Tasks
- Available rake task can be listed with `rake -T` command
- Lists all database tasks from active_record,   rake tasks for gem and custom `exchange_rate_converter:import` and  `exchange_rate_converter:update` for data import and updates.

## How to run the library?
1. Clone/Copy the repository in the desired location.
2. Copy the `config/database_example.yml` to `config/database.yml` and update the details
3. Install the dependencies with `bundle install` command
4. Create database with rake command i.e `rake db:create:all` (Only for SQLite).
5. Run the migration i.e `rake db:migrate`
6.  Also, run the migration for test with addition of 'env' i.e `rake db:migrate ENV=test` (The migration for the test are not executed when we run it for the development)
7. There are a couple of rake tasks for import and update. Import attempts to insert all the records while updates only insert the recent records.
8. Fire up IRB and then `require './lib/exchange_rate_converter.rb'`
9. Following commands are available
  i. ExchangeRateConverter.import
  ii. ExchangeRateConverter.update
  iii. ExchangeRateConverter.convert(120, '2011-03-05')

TODO: Fix the flaw in update logic
There is one flaw in the update logic. Since the record for the recent days are inserted first, there is a possiblity of partial update if the previous insert/update was partial.

