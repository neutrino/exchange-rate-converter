class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table(:currencies) do |t|
      t.date :exchange_date
      t.float :exchange_rate
      t.timestamps
    end

    add_index(:currencies, :exchange_date)
  end
end
