require 'spec_helper'
require 'date'

RSpec.describe Currency do
  describe 'validations' do
    it 'should have exchange_date and exchange rate' do
      currency = Currency.new
      currency.exchange_date = Date.today - 2.days
      currency.exchange_rate = 1.2345
      expect(currency.valid?).to be true
    end

    it 'should be invalid if exchange_date is missing' do
      currency = Currency.new(exchange_rate: 1.2345)
      expect(currency.valid?).to be false
    end

    it 'should be invalid if incorrect date is given' do
      currency = Currency.new(exchange_rate: 1.2345)
      currency.exchange_date = '2002-012-14'
      expect(currency.valid?).to be false
    end

    it 'should be invalid if exchange_rate is missing' do
      currency = Currency.new(exchange_date: Date.today)
      expect(currency.valid?).to be false
    end

    it 'should check the numericality of exchange rate' do
      currency = Currency.new(exchange_date: Date.today)
      currency.exchange_rate = 'abcd'
      expect(currency.valid?).to be false
    end
  end

  describe 'fetch recent exchange data' do
    before(:each) do
      @exchange_rate_today = Currency.create(exchange_rate: 1.2345,
        exchange_date: Date.today)
      @exchange_rate_two = Currency.create(exchange_rate: 1.2345,
        exchange_date: Date.today - 5.days)
      @exchange_rate_three = Currency.create(exchange_rate: 1.2345,
        exchange_date: Date.today - 6.days)
    end

    it 'returns recent date in absence of recent date' do
      expect(Currency.recent_rate).to eq @exchange_rate_today
    end

    it 'returns exchange rate for that particular date' do
      expect(Currency.recent_rate(Date.today - 6.days)).to eq @exchange_rate_three
    end

    it 'returns exhange rate from the earlier days in absence exact date' do
      expect(Currency.recent_rate(Date.today - 2.days)).to eq @exchange_rate_two
    end
  end
end
