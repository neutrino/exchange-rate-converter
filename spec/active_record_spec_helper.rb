#Source: https://gist.github.com/coreyhaines/2068977#file-active_record_spec_helper-rb
require 'active_record'

# connection_info = YAML.load_file("config/database.yml")["test"]
ActiveRecord::Base.configurations = DatabaseTasks.database_configuration
ActiveRecord::Base.establish_connection DatabaseTasks.env.to_sym

RSpec.configure do |config|
  config.around do |example|
    ActiveRecord::Base.transaction do
      example.run
      raise ActiveRecord::Rollback
    end
  end
end
