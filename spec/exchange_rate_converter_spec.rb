require 'spec_helper'

RSpec.describe ExchangeRateConverter do
  describe '#fetch_data' do
    it 'request data from the EXCHANGE_URL and returns it' do
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: File.new('spec/fixtures/data_sample_17.csv'), status: 200)
      data = ExchangeRateConverter.fetch_data
      expect(data).to_not be nil
    end

    it 'returns nil if there was any error' do
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: '', status: 404)
      data = ExchangeRateConverter.fetch_data
      expect(data).to be nil
    end
  end

  describe '#read_csv' do
    it 'read the response and returns csv data' do
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: File.new('spec/fixtures/data_sample_17.csv'), status: 200)
      csv_data = ExchangeRateConverter.read_csv
      expect(csv_data.size).to eq 27
    end
  end

  describe '#import' do
    it 'parses and inserts the currency records in the database' do
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: File.new('spec/fixtures/data_sample_17.csv'), status: 200)
      expect(Currency.count).to eq 0
      ExchangeRateConverter.import
      expect(Currency.count).to eq 22
    end
  end

  describe '#update' do
    it 'adds new records from the newer date' do
      expect(Currency.count).to eq 0
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: File.new('spec/fixtures/data_sample_17.csv'), status: 200)
      ExchangeRateConverter.import
      expect(Currency.count).to eq 22
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: File.new('spec/fixtures/data_sample_17_recent.csv'), status: 200)
      ExchangeRateConverter.update
      expect(Currency.count).to eq 73
    end

    it 'adds new records for blank database' do
      expect(Currency.count).to eq 0
      stub_request(:any, ExchangeRateConverter::EXCHANGE_URL).to_return(body: File.new('spec/fixtures/data_sample_17.csv'), status: 200)
      ExchangeRateConverter.update
      expect(Currency.count).to eq 22
    end
  end

  describe '#valid_date?' do
    it 'validates the date format' do
      expect(ExchangeRateConverter.valid_date?('2011-03-05')).to be true
      expect(ExchangeRateConverter.valid_date?('2011-033-05')).to be false
      expect(ExchangeRateConverter.valid_date?('2011-3-55')).to be false
    end

    it 'shows future dates as invalid date' do
      expect(ExchangeRateConverter.valid_date?('20111-03-05')).to be false
    end
  end

  describe '#valid_amount?' do
    it 'validates the input amount' do
      expect(ExchangeRateConverter.valid_amount?(123)).to be true
      expect(ExchangeRateConverter.valid_amount?(123.5676)).to be true
      expect(ExchangeRateConverter.valid_amount?('abc')).to be false
    end
  end

  describe '#add_exchange_rate' do
    it 'add new currency item' do
      csv_row = ['2016-12-30', 1.0541]
      expect {
          ExchangeRateConverter.add_exchange_rate(csv_row)
        }.to change { Currency.count }.by(1)
    end

    it 'doesnt add new currency item for invalid csv' do
      csv_row = ['2016-120-30', 1.0541]
      expect {
          ExchangeRateConverter.add_exchange_rate(csv_row)
        }.to change { Currency.count }.by(0)
    end

    it 'doesnt add new currency item for duplicate date' do
      csv_row = ['2016-120-30', 1.0541]
      expect {
          ExchangeRateConverter.add_exchange_rate(csv_row)
        }.to change { Currency.count }.by(0)
    end
  end

  describe '#convert' do
    it 'converts the given amount to euro' do
      currency = Currency.create(exchange_rate: 1.2345,
        exchange_date: Date.parse('2011-03-05'))
      converted_amount = ExchangeRateConverter.convert(120, '2011-03-05')
      expect(converted_amount).to eq 120 * currency.exchange_rate
    end

    it 'shows error message for invalid date' do
      Currency.create(exchange_rate: 1.2345,
        exchange_date: Date.parse('2017-1-22'))
      expect {
          ExchangeRateConverter.convert(120, '2017-1-222')
        }.to raise_error(ArgumentError)
    end

    it 'shows error message for invalid amount' do
      expect {
          ExchangeRateConverter.convert('a120', '2017-1-22')
        }.to raise_error(ArgumentError)
    end

    it 'shows relevant message for empty database' do
      expect(ExchangeRateConverter.convert(120,
        '2017-1-22')).to be nil
    end
  end
end
