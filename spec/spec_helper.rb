require 'bundler/setup'
require 'webmock/rspec'
require 'exchange_rate_converter'
require 'active_record_spec_helper'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
