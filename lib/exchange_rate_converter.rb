require 'csv'
require 'date'
require 'net/http'
require_relative 'currency'

class ExchangeRateConverter
  EXCHANGE_URL = "http://sdw.ecb.europa.eu/quickviewexport.do?SERIES_KEY=120.EXR.D.USD.EUR.SP00.A&type=csv".freeze

  class << self
    def fetch_data
      Net::HTTP.get(URI.parse(EXCHANGE_URL))
      rescue Exception => e
        puts e.message
    end

    def read_csv
      data = fetch_data
      return false if data.nil?
      CSV.parse(data)
    end

    def import
      data_csv = read_csv
      return unless data_csv
      # First five lines are header informations and are always same
      # They are removed from the csv data
      data_csv.drop(5).each { |row| add_exchange_rate(row) }
    end

    def update
      data_csv = read_csv
      return unless data_csv
      recent_rate = Currency.recent_rate
      data_csv.drop(5).each do |row|
        exchange_date = Date.parse(row[0])
        # Assumption: The data is always ordered by date
        # break if the new date is already in the system
        break if recent_rate && recent_rate.exchange_date >= exchange_date
        add_exchange_rate(row)
      end
    end

    def valid_amount?(amount)
      amount.is_a?(Float) || amount.is_a?(Integer) rescue false
    end

    def valid_date?(exchange_date)
      (Date.parse(exchange_date) <= Date.today) rescue false
    end

    # Main Methods
    def convert(amount, exchange_date)
      raise ArgumentError unless (valid_amount?(amount) && valid_date?(exchange_date))
      begin
        currency = Currency.recent_rate(exchange_date)
        raise "Empty Database" unless currency
        amount * currency.exchange_rate
      rescue Exception => e
        puts e.message
      end
    end

    def add_exchange_rate(row)
      currency = Currency.new(exchange_date: row[0],
                              exchange_rate: row[1])
      currency.save if currency.valid?
    end
  end
end
