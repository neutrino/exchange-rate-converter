require 'active_record'
require 'date'
require_relative 'active_record_helper'

class Currency < ActiveRecord::Base
  validates :exchange_date, presence: true, uniqueness: true
  validates :exchange_rate, presence: true, numericality: true

  class << self
    # returns the exchange rate for that specific day
    # returns the recent exchange rate if date is not specified
    def recent_rate(published_date = nil)
      selected_date = published_date ? published_date : Date.today
      where('exchange_date <= ? ',
        selected_date).order('exchange_date desc').limit(1).take
    end
  end
end
